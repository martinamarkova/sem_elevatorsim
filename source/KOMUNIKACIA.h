/*
 * KOMUNIKACIA.h
 *
 *  Created on: 4. 1. 2019
 *      Author: M&M
 */


#ifndef KOMUNIKACIA_H_
#define KOMUNIKACIA_H_

#include <KOMPONENTY.h>

#define MAX_VELKOST_SPRAVY 261
#define MAX_VELKOST_DAT	256
#define START_BYTE 0xa0
#define ODOSIELATEL 0x11

char getMyCharBlocking();

class KOMUNIKACIA {
public:
	KOMUNIKACIA();
	virtual ~KOMUNIKACIA();
	char posli_Spravu(char prijimatel, char* data, char velkost);
	char prijmi_Spravu(char* data, char* velkost, char* odosielatel);

private:
	char daj_crc(char* data);
};

#endif /* KOMUNIKACIA_H_ */
