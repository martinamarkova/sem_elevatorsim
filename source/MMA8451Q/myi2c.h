/*
 * myi2c.h
 *
 *  Created on: 23. 11. 2018
 *      Author: M&M
 */

#ifndef MYI2C_H_
#define MYI2C_H_

#include <fsl_i2c.h>

/**
 * trieda my_i2c, zabezpecuje komunikaciu po i2c zbernici
 */
class my_i2c {
private:
	i2c_master_config_t i2cConfig;
public:
	my_i2c();
	virtual ~my_i2c();
	void readI2C(int adresa, uint8_t subadresa, uint8_t* data, int pocet);
	void writeI2C(int adresa, uint8_t* data, int pocet);
};

#endif /* MYI2C_H_ */
