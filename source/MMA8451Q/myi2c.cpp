/*
 * myi2c.cpp
 *
 *  Created on: 23. 11. 2018
 *      Author: M&M
 */

#include "myi2c.h"

/**
 * konstruktor triedy my_i2c
 * inicializacia i2c zbernice
 */
my_i2c::my_i2c() {
	// TODO Auto-generated constructor stub
	I2C_MasterGetDefaultConfig(&i2cConfig);
	I2C_MasterInit(I2C0, &i2cConfig, CLOCK_GetFreq(I2C0_CLK_SRC));
}

/**
 * destruktor triedy my_i2c
 */
my_i2c::~my_i2c() {
	// TODO Auto-generated destructor stub
}

/**
 * funkcia, kt. precita hodnotu z i2c zbernice
 * @param adresa = adresa zariadenie na zbernici
 * @param subadresa = adresa registra, kt. budeme citat
 * @param data = smernik na miesto v pamati, kde ulozime obsah registra
 * @param pocet = velkost dat (pocet bajtov), kt. budeme citat
 */
void my_i2c::readI2C(int adresa, uint8_t subadresa, uint8_t* data, int pocet) {
	i2c_master_transfer_t xfer;
	memset(&xfer, 0, sizeof(xfer));

	xfer.slaveAddress = adresa;
	xfer.direction = kI2C_Read;
	xfer.subaddress = subadresa;
	xfer.subaddressSize = 1;
	xfer.data = &data[0]; // = data
	xfer.dataSize = pocet;
	xfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferBlocking(I2C0, &xfer);
}

/**
 * funkcia, kt. zapise hodnotu do daneho registra
 * @param adresa = adresa zariadenia na i2c zbernici
 * @param data = data[0] = adresa registra
 * 		      od &data[1] = zacinaju uzitocne data
 * @param pocet = velkost vsetkych dat aj s adresou registra
 */
void my_i2c::writeI2C(int adresa, uint8_t* data, int pocet) {
	i2c_master_transfer_t xfer;
	memset(&xfer, 0, sizeof(xfer));

	xfer.slaveAddress = adresa;
	xfer.direction = kI2C_Write;
	xfer.subaddress = data[0];
	xfer.subaddressSize = 1;
	xfer.data = &data[1]; // ++data;
	xfer.dataSize = pocet - 1;
	xfer.flags = kI2C_TransferDefaultFlag;

	I2C_MasterTransferBlocking(I2C0, &xfer);
}

