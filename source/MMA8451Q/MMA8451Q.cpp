/* Copyright (c) 2010-2011 mbed.org, MIT License
*
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software
* and associated documentation files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all copies or
* substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
* BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "MMA8451Q.h"

#define REG_WHO_AM_I      0x0D
#define REG_CTRL_REG_1    0x2A
#define REG_CTRL_REG_4    0x2D
#define REG_CTRL_REG_5    0x2E
#define REG_OUT_X_MSB     0x01
#define REG_OUT_Y_MSB     0x03
#define REG_OUT_Z_MSB     0x05
#define UINT14_MAX        16383

#define REG_CONFIG     	  0x15
#define REG_SOURCE     	  0x16
#define REG_THRESHOLD  	  0x17
#define REG_COUNT	 	  0x18

/**
 * kontruktor triedy MMA8451Q, inicializacia akcelerometra (pre freeFall)
 */
MMA8451Q::MMA8451Q(int addr) {
	m_addr = addr;
	freeFall();	//inicializacia akcelerometra, tak aby vedel poziadat o prerusenie vo freefall
}

/**
 * destruktor triedy MMA8451Q
 */
MMA8451Q::~MMA8451Q() { }

uint8_t MMA8451Q::getWhoAmI() {
    uint8_t who_am_i = 0;
    readRegs(REG_WHO_AM_I, &who_am_i, 1);
    return who_am_i;
}

/**
 * funckia pre zistenie x-os
 */
float MMA8451Q::getAccX() {
    return (float(getAccAxis(REG_OUT_X_MSB))/4096.0);
}

/**
 * funckia pre zistenie y-os
 */
float MMA8451Q::getAccY() {
    return (float(getAccAxis(REG_OUT_Y_MSB))/4096.0);
}

/**
 * funckia pre zistenie z-os
 */
float MMA8451Q::getAccZ() {
    return (float(getAccAxis(REG_OUT_Z_MSB))/4096.0);
}

/**
 * funckia pre vsetky osi
 * @param res = smernik na pole 3 prvkov typu float, kde sa ulozia hodnoty z xyz osi
 */
void MMA8451Q::getAccAllAxis(float * res) {
    res[0] = getAccX();
    res[1] = getAccY();
    res[2] = getAccZ();
}

/**
 * funkcia na nacitavanie suradnicovych registrov
 * @param addr = adresa suradnicoveho registra bud X,Y,Z
 * @return acc = vratenie 16-bitovej hodnoty suradnice
 */
int16_t MMA8451Q::getAccAxis(uint8_t addr) {
    int16_t acc;
    uint8_t res[2];
    readRegs(addr, res, 2);

	acc = (res[0] << 6) | (res[1] >> 2);
    if (acc > UINT14_MAX/2)
        acc -= UINT14_MAX;

    return acc;
}

/**
 * funckia, pre kominukaciu po i2c zbernici
 * @param addr = adresa registra, kt. budeme citat
 * @param data = smernik, kde budu ulozene uzitocne data
 * @param len = dlzka dat pre citanie
 */
void MMA8451Q::readRegs(int addr, uint8_t* data, int len) {
	i2c.readI2C(m_addr, addr, data, len);
}

/**
 * funkcia pre zapisanie uzitocnych dat do registra akcelerometra
 * @param data =  data[0] = adresa registra, do kt. budeme zapisovat
 * 		       od data[1] az po data[len-1] = uzitocne data, kt. ideme zapisat
 * @param len = pocet uzitocnych bajtov
 */
void MMA8451Q::writeRegs(uint8_t* data, int len) {
	i2c.writeI2C(m_addr, data, len);
}

/**
 * funkcia, kt. informuje akcelerometer o obsluzeni freeFall
 * tym, ze precitame REG_SOURCE => tym ho vynulujeme
 */
void MMA8451Q::freeFallInterrupt() {
	uint8_t data = 0;
	readRegs(REG_SOURCE, &data, 1);
}

/**
 * funkcia pre inicializaciu akcelerometra, aby vedel detegovat freeFall a sucastne aby vedel
 * poziadat o prurusenie MCU prostrednictvom INT2
 */
void MMA8451Q::freeFall() {
	uint8_t pom[2];
	pom[0] = REG_CTRL_REG_1;
	pom[1] = 0x20;
	writeRegs(pom, 2);	//nastavenie standby modu

	pom[0] = REG_CONFIG;
	pom[1] = 0xB8;
	writeRegs(pom, 2); //konfiguracia registra pre freefall

	pom[0] = REG_THRESHOLD;
	pom[1] = 0x03;
	writeRegs(pom, 2); //nastavenie threshold

	pom[0] = REG_COUNT;
	pom[1] = 0x06;
	writeRegs(pom, 2); //counter

	pom[0] = REG_CTRL_REG_4;
	pom[1] = 0x04;
	writeRegs(pom, 2); //povolenie freefall interrupt

	pom[0] = REG_CTRL_REG_5;
	pom[1] = 0x00;
	writeRegs(pom, 2); //prerusenie na INT2

	uint8_t pomReg;	//nastavenie aktivneho modu
	readRegs(REG_CTRL_REG_1, &pomReg, 1);
	pomReg |= 0x01;
	pom[0] = REG_CTRL_REG_1;
	pom[1] = pomReg;
	writeRegs(pom, 2);
}


