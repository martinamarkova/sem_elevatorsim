/*
 * CircularBufferUart.h
 *
 *  Created on: 14. 10. 2018
 *      Author: Martin
 */
// prevzate z: https://github.com/hudikm/TPRIS_CIRCULAR_BUFFER.git URL

#ifndef CIRCULARBUFFERUART_H_
#define CIRCULARBUFFERUART_H_
#include <stdio.h>
#include <utils/CircularBuffer.h>

#define _bufferUartSize 100
#define RYCHLOST 57600
extern buffer_t cBuffer;

void CBufferUart_Init();
int getMyChar(uint8_t* output);

#endif /* CIRCULARBUFFERUART_H_ */
