/*
 * komponenty.h
 *
 *  Created on: 4. 1. 2019
 *      Author: M&M
 */

#ifndef KOMPONENTY_H_
#define KOMPONENTY_H_

#include <CircularBufferUart.h>
#include <stdio.h>
#include <utils/CircularBuffer.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"

#define IN_ELEVATOR_BUTTON0 0xb0
#define IN_ELEVATOR_BUTTON1 0xb1
#define IN_ELEVATOR_BUTTON2 0xb2
#define IN_ELEVATOR_BUTTON3 0xb3
#define IN_ELEVATOR_BUTTON4 0xb4
#define OUT_ELEVATOR_BUTTON0 0xc0
#define OUT_ELEVATOR_BUTTON1 0xc1
#define OUT_ELEVATOR_BUTTON2 0xc2
#define OUT_ELEVATOR_BUTTON3 0xc3
#define OUT_ELEVATOR_BUTTON4 0xc4
#define ELEVATOR_CABIN 0xf0
#define EMERGENCY_BREAK 0xf
#define INFORMATION_DISPLAY 0x30
#define OUT_LED0 0x10
#define OUT_LED1 0x11
#define OUT_LED2 0x12
#define OUT_LED3 0x13
#define OUT_LED4 0x14
#define IN_LED0 0x20
#define IN_LED1 0x21
#define IN_LED2 0x22
#define IN_LED3 0x23
#define IN_LED4 0x24
#define LIMIT_SWITCH0 0xe0
#define LIMIT_SWITCH1 0xe1
#define LIMIT_SWITCH2 0xe2
#define LIMIT_SWITCH3 0xe3
#define LIMIT_SWITCH4 0xe4
#define MOTOR 0xf1
#define TERMINAL 0xd0
#define WATCHDOG_TIMER 0xfe

#endif /* KOMPONENTY_H_ */
