/*
 * LPTMR.h
 *
 *  Created on: 15. 1. 2019
 *      Author: M&M
 */

#ifndef MLPTMR_H_
#define MLPTMR_H_

#include "fsl_debug_console.h"
#include "board.h"

#include "fsl_lptmr.h"
#include "fsl_gpio.h"

#include "pin_mux.h"
#include "clock_config.h"

#define casZamknutia 3000000
#define casBlokovaniaVytahu 6000000

void initMyLptTimer();
void startMyLptTimerForDoor();
void startMyLptTimerFreeFall();
char getBlokovaneDvere();
char blokovanyVytah();

#endif /* MLPTMR_H_ */
