/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Sem_ElevatorSIm.cpp
 * @brief   Application entry point.
 */

#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include <KOMPONENTY.h>
#include "KOMUNIKACIA.h"
#include "VYTAH.h"

/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */

int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    initMyLptTimer();
	CBufferUart_Init();

    KOMUNIKACIA komunikacia;
    VYTAH vytah;

    char watchdogReset = 0x01;
    char watchdogResetRegular = 0x00;
    char emerBreakDeac = 0x00;

    char data2[MAX_VELKOST_DAT];
    char velkost;
    char odosielatel;

    komunikacia.posli_Spravu(WATCHDOG_TIMER, &watchdogReset, 1);
    komunikacia.posli_Spravu(EMERGENCY_BREAK, &emerBreakDeac, 1);

    int i = 0;
    while(1) {
    	i++;
    	if (i == 0x2FFF) {
    		komunikacia.posli_Spravu(WATCHDOG_TIMER, &watchdogResetRegular, 1);
    		i=0;
    	}

    	if(blokovanyVytah()) vytah.zablokujVytah(); //od freeFall

		char success = komunikacia.prijmi_Spravu(data2, &velkost, &odosielatel);
		if (!success || blokovanyVytah()) continue; //ak nebola prijata sprava alebo bol blokovany vytah od FreeFall tak sa vraciame na zaciatok cyklu

		vytah.odblokujVytah(); //opatovne rozpohybovanie vytahu ak vytah bol prave odblokovany po FreeFall
		vytah.nastavVstupnuSpravu(data2, velkost, odosielatel);
		vytah.stlaceneTlacidlo();

		vytah.poziciaLimitSwitch();
		vytah.rozpohybovanieVytahu();
    }
}
