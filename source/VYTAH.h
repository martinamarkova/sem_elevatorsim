/*
 * VYTAH.h
 *
 *  Created on: 4. 1. 2019
 *      Author: M&M
 */

#ifndef VYTAH_H_
#define VYTAH_H_
#include "KOMUNIKACIA.h"
#include "KOMPONENTY.h"
#include <mLPTMR.h>

typedef struct sprava {
	char data[MAX_VELKOST_DAT];
	char velkost;
	char adresa;
}Sprava;

typedef enum smer{
	stop=0, hore=1, dole=2
}Smer;

typedef enum poschodie{
	prizemie = '0', prve = '1', druhe = '2', tretie = '3', stvrte = '4'
}Poschodie;

class VYTAH {
private:
	KOMUNIKACIA komunikacia;
	char aktPoschodie;
	Sprava vstupnaSprava;
	char poziadavka[5];
	char vPohybe;
	char smerVytahu;
	char nudzovaBrzdaActive;

public:
	VYTAH();
	virtual ~VYTAH();
	void nastavVstupnuSpravu(char* data, char velkost, char odosielatel);
	char stlaceneTlacidlo();
	int pozicia();
	char poziciaLimitSwitch();
	char rozpohybovanieVytahu();
	char jePoziadavka();
	void odblokujVytah();
	void zablokujVytah();

private:
	void pohybVytahuHore();
	void pohybVytahuDole();
	void pohybVytahuStop();
	void zamknuteDvere();
	void odomknuteDvere();
	void zasvietLed();
	void zhasniLed();
	void zobrazSmer();
	char jePoziadavkaVSmere(uint8_t smerPoziadavky);
};
#endif /* VYTAH_H_ */
