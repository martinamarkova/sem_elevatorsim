/*
 * LPTMR.c
 *
 *  Created on: 15. 1. 2019
 *      Author: M&M
 */

#include <mLPTMR.h>

char blokovaneDvere;
char blokVytah;

/**
 * funkcia pre obsluhu prerusenia od LPTMR timeru
 * ked nastane prerusenie tak glob. premenne blokovaneDvere a blokVytah nastavi na nulu a zastavi pocitanie timeru
 */
extern "C" void LPTMR0_IRQHandler(void) {
    LPTMR_ClearStatusFlags(LPTMR0, kLPTMR_TimerCompareFlag);
    blokovaneDvere = 0;
    blokVytah = 0;
    /*
     * Workaround for TWR-KV58: because write buffer is enabled, adding
     * memory barrier instructions to make sure clearing interrupt flag completed
     * before go out ISR
     */
    LPTMR_StopTimer(LPTMR0);
    __DSB();
    __ISB();
}

/**
 * funkcia, kt. inicializuje LPTMR timer
 * premennu blokovaneDvere nastavi na nulu
 */
void initMyLptTimer(){
	blokovaneDvere = 0;
	lptmr_config_t lptmrConfig;
	LPTMR_GetDefaultConfig(&lptmrConfig);
	/* Initialize the LPTMR */
	LPTMR_Init(LPTMR0, &lptmrConfig);
}

/**
 * funkcia, kt. spusti pocitanie LPTMR timeru pre nastavenie blokovania dveri
 * nastavi stav blokovanych dvier na 1, povoli prerusenia a spusti casovac pre cas blokovania dveri
 */
void startMyLptTimerForDoor(){
	blokovaneDvere = 1;
    /*
     * Set timer period.
     * Note : the parameter "ticks" of LPTMR_SetTimerPeriod should be equal or greater than 1.
    */
    LPTMR_SetTimerPeriod(LPTMR0, USEC_TO_COUNT(casZamknutia, CLOCK_GetFreq(kCLOCK_LpoClk)));

    /* Enable timer interrupt */
    LPTMR_EnableInterrupts(LPTMR0, kLPTMR_TimerInterruptEnable);

    /* Enable at the NVIC */
    EnableIRQ(LPTMR0_IRQn);

    /* Start counting */
    LPTMR_StartTimer(LPTMR0);
}

/**
 * funkcia, kt. spusti pocitanie LPTMR timeru pre nastavenie blokovania vytahu
 * nastavi stav blokovaneho vytahu na 1, povoli prerusenia a spusti casovac pre cas blokovanie vytahu
 */
void startMyLptTimerFreeFall(){
	blokVytah = 1;
    /*
     * Set timer period.
     * Note : the parameter "ticks" of LPTMR_SetTimerPeriod should be equal or greater than 1.
    */
    LPTMR_SetTimerPeriod(LPTMR0, USEC_TO_COUNT(casBlokovaniaVytahu, CLOCK_GetFreq(kCLOCK_LpoClk)));

    /* Enable timer interrupt */
    LPTMR_EnableInterrupts(LPTMR0, kLPTMR_TimerInterruptEnable);

    /* Enable at the NVIC */
    EnableIRQ(LPTMR0_IRQn);

    /* Start counting */
    LPTMR_StartTimer(LPTMR0);
}

/*
 * funkcia, kt. vrati hodnotu blokovanych dvier (ci su blokovane alebo nie)
 * @return 1 = blokovane
 * @return 0 = neblokovane
 */
char getBlokovaneDvere(){
	return blokovaneDvere;
}

/*
 * funkcia, kt. vrati hodnotu blokovaneho vytahu (ci je blokovany alebo nie)
 * @return 1 = blokovane
 * @return 0 = neblokovane
 */
char blokovanyVytah(){
	return blokVytah;
}
