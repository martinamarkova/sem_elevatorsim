/*
 * KOMUNIKACIA.cpp
 *
 *  Created on: 4. 1. 2019
 *      Author: M&M
 */

#include <KOMUNIKACIA.h>

/**
 * konstruktor triedy KOMUNIKACIA
 */
KOMUNIKACIA::KOMUNIKACIA() {
	// TODO Auto-generated constructor stub
}

/**
 * destruktor triedy KOMUNIKACIA
 */
KOMUNIKACIA::~KOMUNIKACIA() {
	// TODO Auto-generated destructor stub
}

/**
 * funkcia, kt. vrati vypocet CRC8
 * @param data = smernik na data, z kt. sa pocita CRC (parameter, kt. vstupuje do funkcie pre vypocet spravnej hodnoty CRC)
 * @return vysledok = vypocitana hodnota CRC
 */
char KOMUNIKACIA::daj_crc(char* data) {
	char crcTable[] = {
			0, 94, 188, 226, 97, 63, 221, 131, 194, 156, 126, 32, 163, 253, 31, 65,
			157, 195, 33, 127, 252, 162, 64, 30, 95, 1, 227, 189, 62, 96, 130, 220,
			35, 125, 159, 193, 66, 28, 254, 160, 225, 191, 93, 3, 128, 222, 60, 98,
			190, 224, 2, 92, 223, 129, 99, 61, 124, 34, 192, 158, 29, 67, 161, 255,
			70, 24, 250, 164, 39, 121, 155, 197, 132, 218, 56, 102, 229, 187, 89, 7,
			219, 133, 103, 57, 186, 228, 6, 88, 25, 71, 165, 251, 120, 38, 196, 154,
			101, 59, 217, 135, 4, 90, 184, 230, 167, 249, 27, 69, 198, 152, 122, 36,
			248, 166, 68, 26, 153, 199, 37, 123, 58, 100, 134, 216, 91, 5, 231, 185,
			140, 210, 48, 110, 237, 179, 81, 15, 78, 16, 242, 172, 47, 113, 147, 205,
			17, 79, 173, 243, 112, 46, 204, 146, 211, 141, 111, 49, 178, 236, 14, 80,
			175, 241, 19, 77, 206, 144, 114, 44, 109, 51, 209, 143, 12, 82, 176, 238,
			50, 108, 142, 208, 83, 13, 239, 177, 240, 174, 76, 18, 145, 207, 45, 115,
			202, 148, 118, 40, 171, 245, 23, 73, 8, 86, 180, 234, 105, 55, 213, 139,
			87, 9, 235, 181, 54, 104, 138, 212, 149, 203, 41, 119, 244, 170, 72, 22,
			233, 183, 85, 11, 136, 214, 52, 106, 43, 117, 151, 201, 74, 20, 246, 168,
			116, 42, 200, 150, 21, 75, 169, 247, 182, 232, 10, 84, 215, 137, 107, 53
	};

	char vysledok = 0;

	vysledok = crcTable[data[0]];
	vysledok = crcTable[vysledok ^ data[1]];

    for(int i = 3; i < data[2]+3; i++) {
    	vysledok  = crcTable[(vysledok ^ data[i])];
    }
    return vysledok;
}

/**
 * funkcia, kt. zabezpeci odoslanie spravy (konkretneho prikazu) pre vytah
 * @param prijimatel = adresa prijimatela
 * @param data = smernik na data, kt. sa maju odoslat (prikaz, kt. ma vytah vykonat)
 * @param velkost = udava size celej posielanej dat
 */
char KOMUNIKACIA::posli_Spravu(char prijimatel, char* data, char velkost) {
	char sprava[MAX_VELKOST_SPRAVY];
	sprava[0] = START_BYTE;
	sprava[1] = prijimatel;
	sprava[2] = ODOSIELATEL;
	sprava[3] = velkost;

	for(int i = 0; i < velkost; i++) {
			sprava[i+4] = data[i];
	}
	sprava[velkost+4] = daj_crc(&sprava[1]);

	for(int i = 0; i < velkost+5; i++) {
		PUTCHAR(sprava[i]);
	}
	return 0;
}

/**
 * funkcia, kt. blokujuco precita znak, pokym nieco neprecita tak caka
 * @param pom = precitany znak
 */
char getMyCharBlocking() {
	char pom;
	while(!getMyChar((uint8_t*)(&pom)));
	return pom;
}

/**
 * funkcia, kt. prijme danu spravu od vytahu
 * @param data = smernik na miesto v pamati, kde sa maju data ulozit
 * @param velkost = (adresa premennej), kde sa sa uklada velkost prijatej dat
 * @param odosielat = adresa odosielatela (smernik na premennu)
 * @return 1 = podarilo sa precitat spravu
 * @return 0 = nepodarilo sa precitat spravu
 */
char KOMUNIKACIA::prijmi_Spravu(char* data, char* velkost, char* odosielatel) {
	char sprava[MAX_VELKOST_SPRAVY];
	char success = getMyChar((uint8_t*)sprava); // getMyChar((uint8_t*)(&sprava[0]));

	if( success && (sprava[0] == START_BYTE) ) {
	sprava[1] = getMyCharBlocking();	//prijimatel (ja)
	*odosielatel = getMyCharBlocking();	//odosielatel
	sprava[3] = getMyCharBlocking();	//velkost
	*velkost = sprava[3];

	for(int i = 0; i < sprava[3]; i++) {
		sprava[i+4] = getMyCharBlocking();
		data[i] = sprava[i+4];
	}
	sprava[sprava[3]+4] = getMyCharBlocking();	//CRC
	return 1;
	}
	return 0;
}
