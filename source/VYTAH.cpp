/*
 * VYTAH.cpp
 *
 *  Created on: 4. 1. 2019
 *      Author: M&M
 */

#include <VYTAH.h>
#include "MMA8451Q/MMA8451Q.h"

MMA8451Q* glob_akcel;

/**
 * konstruktor triedy VYTAH
 */
VYTAH::VYTAH() {
	initMyLptTimer();
	glob_akcel = new MMA8451Q(0x1D);
	NVIC_EnableIRQ(PORTA_IRQn);
	nudzovaBrzdaActive = 0;

	for(int i = 0; i < 5; i++){
		poziadavka[i] = 0;
	}
	vPohybe = 0;
}

/**
 * destruktor triedy VYTAH
 */
VYTAH::~VYTAH() {
	delete glob_akcel;
}

/**
 * fukcia, kt. nastavi hodnoty premennym struktury Sprava (data, velkost, odosielatel)
 * @param data = smernik na prijate data, kt. pridu v hlavnom programe (main)
 * @param velkost = velkost uzitocnych dat
 * @param odosielatel = zdrojova adresa prijateho paketu
 */
void VYTAH::nastavVstupnuSpravu(char* data, char velkost, char odosielatel){
	for(int i=0; i < velkost; i++){
		vstupnaSprava.data[i] = data[i];
	}
	vstupnaSprava.velkost = velkost;
	vstupnaSprava.adresa = odosielatel;
}

/**
 * funkcia, kt. rozpohybuje vytah smerom hore
 */
void VYTAH::pohybVytahuHore() {
	char pohybHore[] = {0x02, 0x00, 0x00 , 0x00 , 0x16};
	komunikacia.posli_Spravu(MOTOR, pohybHore, 0x05);
}

/**
 * funkcia, kt. rozpohybuje vytah smerom dole
 */
void VYTAH::pohybVytahuDole() {
	char pohybDole[] = {0x02, 0xFF, 0xFF , 0xFF , 0xC0};
	komunikacia.posli_Spravu(MOTOR, pohybDole, 0x05);
}

/**
 * funkcia, kt. zastavi pohyb vytahu
 */
void VYTAH::pohybVytahuStop() {
	char pohybStop[] = {0x01};
	komunikacia.posli_Spravu(MOTOR, pohybStop, 0x01);
}

/**
 * funkcia, kt. zamkne dvere na vytahu
 */
void VYTAH::zamknuteDvere() {
	char zamknute[] = {0x01};
	komunikacia.posli_Spravu(ELEVATOR_CABIN, zamknute, 0x01);
}

/**
 * funkcia, kt. odomkne dvere na vytahu
 */
void VYTAH::odomknuteDvere() {
	char odomknute[] = {0x00};
	komunikacia.posli_Spravu(ELEVATOR_CABIN, odomknute, 0x01);
}

/**
 * funkcia, kt. zasvieti ledky na zaklade stlaceneho tlacidla na danom poschodi
 */
void VYTAH::zasvietLed() {
	char svieti[] = {0x01};

	switch(vstupnaSprava.adresa) {
		case OUT_ELEVATOR_BUTTON0:
		case IN_ELEVATOR_BUTTON0:
			komunikacia.posli_Spravu(OUT_LED0, svieti, 0x01);
			komunikacia.posli_Spravu(IN_LED0, svieti, 0x01);
			break;

		case OUT_ELEVATOR_BUTTON1:
		case IN_ELEVATOR_BUTTON1:
			komunikacia.posli_Spravu(OUT_LED1, svieti, 0x01);
			komunikacia.posli_Spravu(IN_LED1, svieti, 0x01);
			break;

		case OUT_ELEVATOR_BUTTON2:
		case IN_ELEVATOR_BUTTON2:
			komunikacia.posli_Spravu(OUT_LED2, svieti, 0x01);
			komunikacia.posli_Spravu(IN_LED2, svieti, 0x01);
			break;

		case OUT_ELEVATOR_BUTTON3:
		case IN_ELEVATOR_BUTTON3:
			komunikacia.posli_Spravu(OUT_LED3, svieti, 0x01);
			komunikacia.posli_Spravu(IN_LED3, svieti, 0x01);
			break;

		case OUT_ELEVATOR_BUTTON4:
		case IN_ELEVATOR_BUTTON4:
			komunikacia.posli_Spravu(OUT_LED4, svieti, 0x01);
			komunikacia.posli_Spravu(IN_LED4, svieti, 0x01);
			break;
		default:
			break;
	}
}

/**
 * funkcia, kt. zhasne ledky po prichode vytahu na dane zavolane poschodie
 */
void VYTAH::zhasniLed() {
	char nesvieti[] = {0x00};
	if(aktPoschodie == prizemie){
		komunikacia.posli_Spravu(OUT_LED0, nesvieti, 0x01);
		komunikacia.posli_Spravu(IN_LED0, nesvieti, 0x01);
	}
	if(aktPoschodie == prve){
		komunikacia.posli_Spravu(OUT_LED1, nesvieti, 0x01);
		komunikacia.posli_Spravu(IN_LED1, nesvieti, 0x01);
	}
	if(aktPoschodie == druhe){
		komunikacia.posli_Spravu(OUT_LED2, nesvieti, 0x01);
		komunikacia.posli_Spravu(IN_LED2, nesvieti, 0x01);
	}
	if(aktPoschodie == tretie){
		komunikacia.posli_Spravu(OUT_LED3, nesvieti, 0x01);
		komunikacia.posli_Spravu(IN_LED3, nesvieti, 0x01);
	}
	if(aktPoschodie == stvrte){
		komunikacia.posli_Spravu(OUT_LED4, nesvieti, 0x01);
		komunikacia.posli_Spravu(IN_LED4, nesvieti, 0x01);
	}
}

/**
 * funckia, kt. zobrazi smer vytahu (smer smerom hore/dole)
 */
void VYTAH::zobrazSmer(){
	char stopSmer[] = {0x00, aktPoschodie};
	char smerHore[] = {0x01, aktPoschodie};
	char smerDole[] = {0x02, aktPoschodie};

	if(!jePoziadavka()){
		komunikacia.posli_Spravu(INFORMATION_DISPLAY, stopSmer, 0x02);
	}else if(smerVytahu == hore){
		komunikacia.posli_Spravu(INFORMATION_DISPLAY, smerHore, 0x02);
	}else if(smerVytahu == dole){
		komunikacia.posli_Spravu(INFORMATION_DISPLAY, smerDole, 0x02);
	}
}

/**
 * funkcia, kt. na zaklade prijatej spravy (adresy zdroja paketu) stlaceneho tlacidla sa rozhodne, kt. button ma obsluzit a vykonat prislusne funkcie
 */
char VYTAH::stlaceneTlacidlo() {
	switch(vstupnaSprava.adresa) {
	//--------------------------------------------------
		case OUT_ELEVATOR_BUTTON0:
		case IN_ELEVATOR_BUTTON0:

			if(!vPohybe && !jePoziadavka()) { //vytah sa nehybe a nie je ziadna poziadavka
				if(pozicia() > 0){
					if(!getBlokovaneDvere()) pohybVytahuDole();
					zamknuteDvere();
					smerVytahu = dole;
					zobrazSmer();
				}else if(pozicia() <= 0){
					return 1;
				}
				if(!getBlokovaneDvere()) vPohybe = 1;	//nastavenie vytahu do pohybu
			}
			poziadavka[0] = 1;
			zasvietLed();
			break;
	//--------------------------------------------------
		case OUT_ELEVATOR_BUTTON1:
		case IN_ELEVATOR_BUTTON1:

			if(!vPohybe && !jePoziadavka()) {
				if(pozicia() > 1){
					if(!getBlokovaneDvere()) pohybVytahuDole();
					zamknuteDvere();
					smerVytahu = dole;
					zobrazSmer();
				}else if(pozicia() < 1){
					if(!getBlokovaneDvere()) pohybVytahuHore();
					zamknuteDvere();
					smerVytahu = hore;
					zobrazSmer();
				}else if(pozicia() == 1){
					return 1;
				}
				if(!getBlokovaneDvere()) vPohybe = 1;
			}
			poziadavka[1] = 1;
			zasvietLed();
			break;
	//--------------------------------------------------
		case OUT_ELEVATOR_BUTTON2:
		case IN_ELEVATOR_BUTTON2:

			if(!vPohybe && !jePoziadavka()) {
				if(pozicia() > 2){
					if (!getBlokovaneDvere()) pohybVytahuDole();
					zamknuteDvere();
					smerVytahu = dole;
					zobrazSmer();
				}else if(pozicia() < 2){
					if(!getBlokovaneDvere()) pohybVytahuHore();
					zamknuteDvere();
					smerVytahu = hore;
					zobrazSmer();
				}else if(pozicia() == 2){
					return 1;
				}
				if(!getBlokovaneDvere()) vPohybe = 1;
			}
			poziadavka[2] = 1;
			zasvietLed();
			break;
	//--------------------------------------------------
		case OUT_ELEVATOR_BUTTON3:
		case IN_ELEVATOR_BUTTON3:

			if(!vPohybe && !jePoziadavka()) {
				if(pozicia() > 3){
					if (!getBlokovaneDvere()) pohybVytahuDole();
					zamknuteDvere();
					smerVytahu = dole;
					zobrazSmer();
				}else if(pozicia() < 3){
					if(!getBlokovaneDvere()) pohybVytahuHore();
					zamknuteDvere();
					smerVytahu = hore;
					zobrazSmer();
				}else if(pozicia() == 3){
					return 1;
				}
				if(!getBlokovaneDvere()) vPohybe = 1;
			}
			poziadavka[3] = 1;
			zasvietLed();
			break;
	//--------------------------------------------------
		case OUT_ELEVATOR_BUTTON4:
		case IN_ELEVATOR_BUTTON4:

			if(!vPohybe && !jePoziadavka()){
				if(pozicia() > 4 || pozicia() == 4){
					return 1;
				}else if(pozicia() < 4 ){
					if(!getBlokovaneDvere()) pohybVytahuHore();
					zamknuteDvere();
					smerVytahu = hore;
					zobrazSmer();
				}
				if(!getBlokovaneDvere()) vPohybe = 1;
			}
			poziadavka[4] = 1;
			zasvietLed();
			break;
	//--------------------------------------------------
		default:
			break;
	}
	return 0;
}

/**
 * funkcia, kt. nam vrati aktualnu poziciu vytahu, na kt. poschodi sa vytah nachadza
 * @return 0 = vrati cislo poschodia 0
 * @return 1 = vrati cislo poschodia 1
 * @return 2 = vrati cislo poschodia 2
 * @return 3 = vrati cislo poschodia 3
 * @return 4 = vrati cislo poschodia 4
 */
int VYTAH::pozicia() {
	char data = 0x03; //0x03 = aktualny pozicia
	char data2[MAX_VELKOST_DAT];
	char velkost;
	char odosielatel;
	komunikacia.posli_Spravu(MOTOR, &data, 1);
	double poschodie;

	while(1){
		komunikacia.prijmi_Spravu(data2, &velkost, &odosielatel);
		if(odosielatel == MOTOR){
			memcpy(&poschodie, data2, sizeof(double));
			break;
		}
	}
	if(poschodie > -200.605) return 4;
	if(poschodie > -429.485) return 3;
	if(poschodie > -668.805) return 2;
	if(poschodie > -894.805) return 1;
	return 0;
}

/**
 * ak zdroj spravy je Limit Switch, tak funkcia na zaklade zistenia pozicie LimitSwitchu a ci je este poziadavka na dane poschodie,
 * nastavi dane atributy vytahu a vykona prislusne funckie
 */
char VYTAH::poziciaLimitSwitch(){
	switch(vstupnaSprava.adresa){
	case LIMIT_SWITCH0:
		aktPoschodie = prizemie;
		zobrazSmer();

		if(poziadavka[0]){
			pohybVytahuStop();
			vPohybe = 0;
			poziadavka[0] = 0;
			zhasniLed();
			startMyLptTimerForDoor();
			odomknuteDvere();
		}
		break;

	case LIMIT_SWITCH1:
		aktPoschodie = prve;
		zobrazSmer();

		if(poziadavka[1]){
			pohybVytahuStop();
			vPohybe = 0;
			poziadavka[1] = 0;
			zhasniLed();
			startMyLptTimerForDoor();
			odomknuteDvere();
		}
		break;

	case LIMIT_SWITCH2:
		aktPoschodie = druhe;
		zobrazSmer();
		if(poziadavka[2]){
			pohybVytahuStop();
			vPohybe = 0;
			poziadavka[2] = 0;
			zhasniLed();
			startMyLptTimerForDoor();
			odomknuteDvere();
		}
		break;

	case LIMIT_SWITCH3:
		aktPoschodie = tretie;
		zobrazSmer();
		if(poziadavka[3]){
			pohybVytahuStop();
			vPohybe = 0;
			poziadavka[3] = 0;
			zhasniLed();
			startMyLptTimerForDoor();
			odomknuteDvere();
		}
		break;

	case LIMIT_SWITCH4:
		aktPoschodie = stvrte;
		zobrazSmer();
		if(poziadavka[4]){
			pohybVytahuStop();
			vPohybe = 0;
			poziadavka[4] = 0;
			smerVytahu = stop;
			zhasniLed();
			startMyLptTimerForDoor();
			odomknuteDvere();
		}
		break;
	default:
		break;
	}
	return 0;
}

/**
 * funckia, kt. na zaklade overenia ci ma poziadavku na vytah a sucasne nie je vytah v pohybe, nastavi prislusne atributy vytahu
 * a rozpohybuje vytah takym smerom ako mu prisla poziadavka
 */
char VYTAH::rozpohybovanieVytahu(){
	if(getBlokovaneDvere()) return 1;

	if(jePoziadavka() && !vPohybe){ //iba v pripade ked je poziadavka ale vytah nie je v pohybe
		if(smerVytahu == hore){
			if(jePoziadavkaVSmere(hore)){
				pohybVytahuHore();
				zamknuteDvere();
				zobrazSmer();
			}else{
				pohybVytahuDole();
				zamknuteDvere();
				smerVytahu = dole;
				zobrazSmer();
			}
			vPohybe = 1;
		}

		if(smerVytahu == dole){
			if(jePoziadavkaVSmere(dole)){
				pohybVytahuDole();
				zamknuteDvere();
				zobrazSmer();
			}else{
				pohybVytahuHore();
				zamknuteDvere();
				smerVytahu = hore;
				zobrazSmer();
			}
			vPohybe = 1;
		}

		if(smerVytahu == stop){
			if(jePoziadavkaVSmere(dole)){
				pohybVytahuDole();
				zamknuteDvere();
				smerVytahu = dole;
				zobrazSmer();
			}else{
				pohybVytahuHore();
				zamknuteDvere();
				smerVytahu = hore;
				zobrazSmer();
			}
		}
	}
	return 0;
}

/**
 * funkcia, kt. zisti ci je este nejaka poziadavka na vytah, alebo nie je
 * @return vrati true ak je nejaka poziadavka
 */
char VYTAH::jePoziadavka(){
	return (poziadavka[0] || poziadavka[1] || poziadavka[2] || poziadavka[3] || poziadavka[4]);
}

/**
 * funkcia, kt. overuje ci v danom smere je nejaka poziadavka
 * @param smerPoziadavky = parameter, kt. urcuje v ktorom smere hladame poziadavku
 */
char VYTAH::jePoziadavkaVSmere(uint8_t smerPoziadavkyp){
	char poziciaVytahu = pozicia();

	if(smerPoziadavkyp == dole){
		for(int i=0; i<poziciaVytahu; i++){
			if(poziadavka[i]) return 1;
		}
	}else if(smerPoziadavkyp == hore){
		for(int i=4; i>poziciaVytahu; i--){
			if(poziadavka[i]) return 1;
		}
	}
	return 0;
}

/**
 * funkcia, kt. v pripade ze bola aktivovana nudzova brzda (FreeFall) a uplynul casovac blokovania vytahu, opatovne povoli fungovanie vytahu
 * spustac tejto funkcie je zmena na 0, potom funkcia blokuje samu seba
 */
void VYTAH::odblokujVytah(){
	if(nudzovaBrzdaActive){
		char emerBreakDeac = 0x00;
		nudzovaBrzdaActive = 0;
		komunikacia.posli_Spravu(EMERGENCY_BREAK, &emerBreakDeac, 1);
		rozpohybovanieVytahu();
	}
}

/**
 * funkcia, kt. zablokuje fungovanie vytahu, ked nastane aktivovanie nudzovej brzdy (FreeFall)
 * spustac tejto funkcie je zmena na 1, potom funkcia blokuje samu seba
 */
void VYTAH::zablokujVytah(){
	if(!nudzovaBrzdaActive) {
		char emerBreakActive = 0x01;
		vPohybe = 0;
		nudzovaBrzdaActive = 1;
		pohybVytahuStop();
		komunikacia.posli_Spravu(EMERGENCY_BREAK, &emerBreakActive, 1);
	}
}

/**
 * funkcia, kt. sa zavola ked nastane prerusenie od GPIOA, v nasom pripade ked nastane prerusenie od FreeFall
 */
extern "C" void PORTA_IRQHandler(void) {
	GPIO_ClearPinsInterruptFlags(GPIOA, (1<<15)); 	//vynulovanie poziadavky o prerusenie od pinu 15 na gpioa
	glob_akcel->freeFallInterrupt();
	startMyLptTimerFreeFall();
}
